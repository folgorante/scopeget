#!/usr/bin/env python2.7

# Copyright (c) 2019 Marco Bisetto <marcobisetto@folgorante.net>
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from sys import argv, stderr, stdout
import serial, datetime, time

def ack(ser):
    """Read serial port for acknowledge.
    """
    acks = ser.read(2)
    if acks != "0\r":
        stderr.write("error: %s\n" % (acks,))
        if acks == "1\r":
            stderr.write("Syntax Error\n")
        elif acks == "2\r":
            stderr.write("Execution Error\n")
        elif acks == "3\r":
            stderr.write("Synchronization Error\n")
        elif acks == "4\r":
            stderr.write("Communication Error\n")
        elif acks < 1 or acks > 4:
            stderr.write("Unknown Acknowledge\n")
            #fi

        ser.close()
        exit(1)
        #fi
    #fed

def openserial(prt, baud, tmout):
    """Open serial port.

    Return: serial object.
    """
    ser = serial.Serial(
        port = prt,
        baudrate = baud,
        bytesize = serial.EIGHTBITS,
        parity = serial.PARITY_NONE,
        stopbits = serial.STOPBITS_ONE,
        timeout = tmout, #set a timeout value, None to wait forever
        xonxoff = 0, #software flow control
        rtscts = 0, #RTS/CTS flow control
        writeTimeout = 3) #set a timeout for writes
    return ser
    #fed
    
if len(argv) < 3:
    stderr.write("""
    REMOTE CONTROL AND PROGRAMMING REFERENCE
    for the FLUKE 123 industrial ScopeMeter
    ** Overview of commands **
    
    COMMAND NAME       COMMAND
                        HEADER
    --------------------------------------------------------------------------
    AUTO SETUP            AS As pressing the AUTO key.
                             Select the items that are affected by the
                             AUTO SET procedure via the USER OPTIONS key.
    ARM TRIGGER           AT For single shot measurements.
                             When the AT command is given while an acquisition
                             is in progress, this acquisition is aborted and
                             the trigger system is rearmed.
    CLEAR MEMORY          CM Clear all saved setups, waveforms, and screens
                             from memory.
    CPL VERSION QUERY     CV To do.
    DEFAULT SETUP         DS Wait for at least 2 seconds after the
                             <acknowledge> reply has been received, to let
                             the ScopeMeter settle itself before you send the
                             next command.
    GET DOWN              GD Switch off.
    GO TO LOCAL           GL To do.
    GO TO REMOTE          GR To do.
    IDENTIFICATION        ID
    INSTRUMENT STATUS     IS To do.
    PROGRAM COMMUNICATION PC Program the baud rate. 1200|2400|4800|9600|19200
    PROGRAM SETUP         PS Proprietary binary format.
    QUERY MEASUREMENT     QM To do.
    QUERY PRINT           QP Dump screen (Epson FX, LQ; Laser Jet; Desk Jet;
                             PostScript)
    QUERY SETUP           QS Proprietary binary format.
    QUERY WAVEFORM        QW
    READ DATE             RD
    RESET INSTRUMENT      RI To do.
    RECALL SETUP          RS Recall an internally stored setup. This setup
                             must have been stored in the ScopeMeter manually
                             or with the SS (Save Setup) command.
                             The new setup is active when you have received
                             the <acknowledge> response.
    READ TIME             RT
    SWITCH ON             SO If there is no power adapter connected, the
                             instrument can only be switched on manually
                             by pressing the Power ON/OFF key.
    SAVE SETUP            SS Save the present setup in one of the battery
                             backuped instrument registers.
    STATUS QUERY          ST To do.
    TRIGGER ACQUISITION   TA Trigger an acquisition. This command acts
                             as a hardware trigger to start a new
                             acquisition. In SINGLE shot acquisition mode
                             the trigger system must have been armed with
                             the AT (Arm Trigger) command.
    WRITE DATE            WD
    WRITE TIME            WT
    --------------------------------------------------------------------------
    fluke.py serial_port command [arg]

    switch on = on
    get down = gd = off
    identification = id
    auto setup = as
    default setup = ds
    arm trigger = at
    trigger acquisition = ta
    read datetime = getnow
    set datetime = setnow
    query print (PostScript) = qp
    query A waveform normal = qa
    query A waveform minmax = qam
    query B waveform normal = qb
    query B waveform minmax = qbm
    recall setup = rs 1...10
    save setup = ss 1...10
    clear memory = cm
    \n""")
    exit(1)
    #fi

port = argv[1] #name of serial device

command = argv[2]
if command == "as": # auto setup
    ser = openserial(port, 1200, 5)
    ser.write("AS\r")
    ack(ser)
    ser.close()
elif command == "ds": # default setup
    ser = openserial(port, 1200, 5)
    ser.write("DS\r")
    ack(ser)
    ser.close()
    time.sleep(2)
elif command == "at": # arm trigger
    ser = openserial(port, 1200, 5)
    ser.write("AT\r")
    ack(ser)
    ser.close()
elif command == "cm": # clear memory
    ser = openserial(port, 1200, 5)
    ser.write("CM\r")
    ack(ser)
    ser.close()
elif command == "on": # switch on
    ser = openserial(port, 1200, 5)
    ser.write("SO\r")
    ack(ser)
    ser.close()
elif command == "off" or command == "gd": # get down
    ser = openserial(port, 1200, 5)
    ser.write("GD\r")
    ack(ser)
    ser.close()
elif command == "id": # identification
    ser = openserial(port, 1200, 5)
    ser.write("ID\r")
    ack(ser)
    rep = ser.read(1000)
    stdout.write(rep + "\n")
    ser.close()
elif command == "ta": # trigger acquisition
    ser = openserial(port, 1200, 5)
    ser.write("TA\r")
    ack(ser)
    ser.close()
elif command == "getnow": # read date, read time
    ser = openserial(port, 1200, 5)
    ser.write("RD\r")
    ack(ser)
    rep = ser.read(11)
    stdout.write(rep + "\n")
    ser.write("RT\r")
    ack(ser)
    rep = ser.read(9)
    stdout.write(rep + "\n")
    ser.close()
elif command == "setnow": # write date, write time
    ser = openserial(port, 1200, 5)
    t = datetime.datetime.now()
    cmd = "WD %d,%d,%d\r" % (t.year, t.month, t.day)
    stderr.write(cmd + "\n")
    ser.write(cmd)
    ack(ser)
    time.sleep(1)
    cmd = "WT %d,%d,%d\r" % (t.hour, t.minute, t.second)
    stderr.write(cmd + "\n")
    ser.write(cmd)
    ack(ser)
    ser.close()
elif command == "qp": # query print
    ser = openserial(port, 1200, 5)
    ser.write("PC 19200\r")
    ack(ser)
    ser.close()
    ser = openserial(port, 19200, 50)
    ser.write("QP 0,3\r")
    ack(ser)
    stdout.write(ser.read(100000))
    ser.write("PC 1200\r")
    ack(ser)
    ser.close()
elif command == "qa": # query waveform
    ser = openserial(port, 1200, 25)
    ser.write("QW 11\r")
    ack(ser)
    stdout.write(ser.read(5000))
    ser.close()
elif command == "qam": # query waveform
    ser = openserial(port, 1200, 25)
    ser.write("QW 10\r")
    ack(ser)
    stdout.write(ser.read(5000))
    ser.close()
elif command == "qb": # query waveform
    ser = openserial(port, 1200, 25)
    ser.write("QW 21\r")
    ack(ser)
    stdout.write(ser.read(5000))
    ser.close()
elif command == "qbm": # query waveform
    ser = openserial(port, 1200, 25)
    ser.write("QW 20\r")
    ack(ser)
    stdout.write(ser.read(5000))
    ser.close()
elif command == "rs": # recall setup
    try:
        i = int(argv[3])
    except:
        stderr.write("Wrong argument, abort.\n")
        exit(1)
        #yrt
    if i < 1 or i > 10:
        stderr.write("Wrong argument, setup must be between 1 and 10.\n")
        exit(1)
        #fi
    ser = openserial(port, 1200, 5)
    ser.write("RS %d\r" % (i,))
    ack(ser)
    ser.close()
elif command == "ss": # save setup
    try:
        i = int(argv[3])
    except:
        stderr.write("Wrong argument, abort.\n")
        ser.close()
        exit(1)
        #yrt
    if i < 1 or i > 10:
        stderr.write("Wrong argument, setup must be between 1 and 10.\n")
        ser.close()
        exit(1)
        #fi
    ser = openserial(port, 1200, 5)
    ser.write("SS %d\r" % (i,))
    ack(ser)
    ser.close()
    #fi
