#!/bin/bash
tty=/dev/ttyUSB0
echo Query A
../scopeget.py $tty qa > qa.dat
echo Query AM
../scopeget.py $tty qam > qam.dat
echo Query B
../scopeget.py $tty qb > qb.dat
echo Query BM
../scopeget.py $tty qbm > qbm.dat
echo Convert to tsv
../scopetotsv.py < qa.dat > qa.tsv
../scopetotsv.py < qam.dat > qam.tsv
../scopetotsv.py < qb.dat > qb.tsv
../scopetotsv.py < qbm.dat > qbm.tsv
echo Plot
gnuplot gaambbm.gnuplot
