set terminal png
set output "volt.png"
set grid
set title "volt"
set xlabel "s"
set ylabel "V"
# plot channel A and channel B trace and min max
plot "qa.tsv" u 1:2 w l title "ch A", \
"qam.tsv" u 1:2 w l notitle linecolor "gray80", \
"qam.tsv" u 1:3 w l notitle linecolor "gray80", \
"qb.tsv" u 1:2 w l title "ch B", \
"qbm.tsv" u 1:2 w l notitle linecolor "gray80", \
"qbm.tsv" u 1:3 w l notitle linecolor "gray80"
