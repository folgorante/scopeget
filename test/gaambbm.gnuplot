# plot channel A and channel B trace and min max
set xlabel "s"
set ylabel "V"
plot "qa.tsv" u 1:2 w l title "ch A", \
"qam.tsv" u 1:2 w l notitle linecolor "gray80", \
"qam.tsv" u 1:3 w l notitle linecolor "gray80", \
"qb.tsv" u 1:2 w l title "ch B", \
"qbm.tsv" u 1:2 w l notitle linecolor "gray80", \
"qbm.tsv" u 1:3 w l notitle linecolor "gray80"
pause -1
