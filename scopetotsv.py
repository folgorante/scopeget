#!/usr/bin/env python2.7

# Copyright (c) 2019 Marco Bisetto <marcobisetto@folgorante.net>

"""scopetotsv reads dump file of fluke waveform data from stdin.
Writes tsv file to stdout.
Prints messages on stderr.
"""
from sys import stderr, stdin
from struct import *

d = stdin.read()
stderr.write("%d bytes read\n" % (len(d)))

init = d[:2]
stderr.write("%s\n" % (init,))
if init != b"#0":
     stderr.write("wrong format\n")
     exit(1)
     #fi
(block_header, block_length) = unpack("!BH", d[2:5])
stderr.write("block_header: %d\nblock_length: %d\n" % (block_header, block_length))
(trace_process,) = unpack("B", d[5])
if trace_process == 1:
     stderr.write("<normal> = 1 No processing\n")
elif trace_process == 2:
     stderr.write("""<average> = 2
     The trace is the result of the
     averaging of multiple traces (equal to
     the SMOOTH function in manual mode)\n""")
elif trace_process == 3:
     stderr.write("""<envelope> = 3
     The trace is the result of the
     envelope process (equal to the
     ENVELOPE function in manual mode)\n""")
else:
     stderr.write("Unknown trace_process: %d\n" % (trace_process,))
     #fi
(trace_result,) = unpack("B", d[6])
if trace_result == 1:
     stderr.write("""<acquisition> = 1
     The trace is a direct result of the
     trace acquisition.\n""")
elif trace_result == 2:
     stderr.write("""<trend_plot> = 2
     The trace is a result of the TrendPlot
     function (recording numerical results).\n""")
elif trace_result == 3:
     stderr.write("""<touch_hold> = 3
     The trace is a copy of the acquisition
     trace. The copy is activated by the
     Touch Hold function of the instrument.\n""")
else:
     stderr.write("Unknown trace_result: %d\n" % (trace_result,))
     #fi
(misc_setup,) = unpack("B", d[7])
if misc_setup & 128:
     stderr.write("DC coupling.\n")
else:
     stderr.write("AC coupling.\n")
     #fi
unit = ["volt", "ampere", "ohm", "farad", "second", "hertz", "degree", "degree_celsius", "degree_farenheit", "percentage", "dBm50", "dBm600", "dBV", "dBA"]
(y_unit, x_unit) = unpack("BB", d[8:10])
if y_unit > 0 and y_unit < 19:
     stderr.write("y_unit: %s\n" % (unit[y_unit - 1],))
else:
     stderr.write("unknown y_unit %d\n" % (y_unit,))
     #fi
if x_unit > 0 and y_unit < 19:
     stderr.write("x_unit: %s\n" % (unit[x_unit - 1],))
else:
     stderr.write("unknown x_unit %d\n" % (x_unit,))
     #fi

(y_zero_mantisse, y_zero_exponent) = unpack("!hb", d[10:13])
y_zero = y_zero_mantisse * 10 ** y_zero_exponent
stderr.write("y_zero: %f\n" % (y_zero,))
(x_zero_mantisse, x_zero_exponent) = unpack("!hb", d[13:16])
x_zero = x_zero_mantisse * 10 ** x_zero_exponent
stderr.write("x_zero: %f\n" % (x_zero,))
(y_resolution_mantisse, y_resolution_exponent) = unpack("!hb", d[16:19])
y_resolution = y_resolution_mantisse * 10 ** y_resolution_exponent
stderr.write("y_resolution_mantisse: %d\t" % (y_resolution_mantisse,))
stderr.write("y_resolution_exponent: %d\n" % (y_resolution_exponent,))
stderr.write("y_resolution: %f\n" % (y_resolution,))
(x_resolution_mantisse, x_resolution_exponent) = unpack("!hb", d[19:22])
x_resolution = x_resolution_mantisse * 10 ** x_resolution_exponent
stderr.write("x_resolution_mantisse: %d\t" % (x_resolution_mantisse,))
stderr.write("x_resolution_exponent: %d\n" % (x_resolution_exponent,))
stderr.write("x_resolution: %f\n" % (x_resolution,))
stderr.write("datetime: %s\n" % (d[22:36],))
sum = 0
for x in d[5:36]:
     sum += ord(x)
     #rof
stderr.write("check_sum: %d = %d\n" % (ord(d[36]), sum & 0xff))
stderr.write("%s\n" % (d[37:40],))
b = 0x26 # setup ended, base for values
init = d[b:b+2]
if init != "#0":
     stderr.write("wrong format\n")
     exit(1)
     #fi
(block_header, block_length) = unpack("!BH", d[b+2:b+5])
stderr.write("block_header: %d\n" % (block_header,))
stderr.write("block_length: %d bytes\n" % (block_length,))
(sample_format,) = unpack("B", d[b+5])
if sample_format & 128:
    signed = True
    stderr.write("signed\n")
else:
    signed = False
    stderr.write("unsigned\n")
#fi
if sample_format & 64:
    minmax = True
    stderr.write("minmax\n")
else:
    minmax = False
    stderr.write("normal\n")
#fi
sample_length = sample_format & 7
stderr.write("sample_length: %d\n" % (sample_length,))
if signed and not minmax and sample_length == 2:
     print "s\t%s\tsample" % (unit[y_unit - 1],)
     (overload, underload, invalid, n_samples) = unpack("!hhhH", d[b+6:b+14])
     stderr.write("overload: %d\n" % (overload,))
     stderr.write("underload: %d\n" % (underload,))
     stderr.write("invalid: %d\n" % (invalid,))
     stderr.write("n_samples: %d\n" % (n_samples,))
     for i in range(n_samples):
          offset = i * sample_length;
          (sample_value,) = unpack("!h", d[b+14+offset:b+14+offset+sample_length])
          x_float_value = x_zero + x_resolution * i
          y_float_value = y_zero + y_resolution * sample_value
          print "%e\t%e\t%d" % (x_float_value, y_float_value, sample_value)
          #rof
elif not signed and minmax and sample_length == 1:
     print "s\t%smin\t%smax\tsamplemin\tsamplemax" % (unit[y_unit - 1], unit[y_unit - 1])
     (overload, underload, invalid, n_samples) = unpack("!BBBH", d[b+6:b+11])
     stderr.write("overload: %d\n" % (overload,))
     stderr.write("underload: %d\n" % (underload,))
     stderr.write("invalid: %d\n" % (invalid,))
     stderr.write("n_samples: %d\n" % (n_samples,))
     for i in range(n_samples):
          offset = i * sample_length * 2 # min,max
          (sample_min, sample_max) = unpack("BB", d[b+11+offset:b+11+offset+2])
          x_float = x_zero + x_resolution * i
          y_min_float = y_zero + y_resolution * sample_min
          y_max_float = y_zero + y_resolution * sample_max
          print "%e\t%e\t%e\t%d\t%d" % (x_float, y_min_float, y_max_float, sample_min, sample_max)
          #rof
     #fi
sum = 0
for x in d[b+5:-2]:
     sum += ord(x)
     #rof
stderr.write("check_sum: %d = %d\n" % (ord(d[-2]), sum & 0xff))
