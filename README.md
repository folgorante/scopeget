# scopeget Fluke ScopeMeter Data Download

**scopeget.py** sends commands to the Fluke ScopeMeter via serial
interface and outputs data to standard out.

**scopetotsv.py** converts data from the ScopeMeter binary format to
Tab Separated Values text.

☛ These programs have been tested with a Fluke 125 ScopeMeter.

The `tsv` files can be easily imported in a spreadsheet.

### Example Usage

    ./scopeget.py /dev/ttyUSB0 on
    ./scopeget.py /dev/ttyUSB0 qa | ./scopetotsv.py > qa.tsv
    ./scopeget.py /dev/ttyUSB0 off

### Plotting tsv with gnuplot

In directory `test` there are simple template files to plot the
data using gnuplot.

### Query Print

In directory `testprint` there is an example of how to download the
printscreen from the ScopeMeter in PostScript and convert it to PNG
using inkscape.
