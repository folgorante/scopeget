#!/bin/bash
echo Query Print
../scopeget.py /dev/ttyUSB0 qp > display.ps
echo Convert PostScript to PNG
inkscape display.ps -z --export-dpi=150 --export-area-drawing --export-png=display.png
